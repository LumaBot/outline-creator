#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 17 14:34:51 2020
Last updated on Apr 17 14:34:51 2020
@author: LumaBot
Description: Converts images w/ a limited color palette into a new png showing only outlines of color sections
"""

#-------------------------------#
#             Setup             #
#-------------------------------#
import os
from PIL import Image
from colormath.color_objects import sRGBColor, LabColor
from colormath.color_conversions import convert_color
from colormath.color_diff import delta_e_cie2000
import importlib
from typing import List
import numpy

#-------------------------------#
#       Function Defintions     #
#-------------------------------#

# Fixes patch_asscalar deprecation
def patch_asscalar( a ):
    return a.item()

# Removes temporary files from given directory
def purge_directory( directory ):
    try:
        directory.remove( '__pycache__' )
    except:
        pass
    try:
        directory.remove( '.DS_Store' )
    except:
        pass
    finally:
        return directory

# Defines file selection menu
def print_directory( file_list: List[str] ) -> None:
    print()
    for name in file_list:
        file_index = file_list.index( name )
        print( '{}: {}'.format( file_index + 1, name ) )

# Defines file name selection
def file_name_selection( directory_list, prompt ):
    print_directory( directory_list )
    user_input = int( input( '\n{}'.format( prompt ) ) )
    if user_input < 1:
        raise IndexError
    file_name = directory_list[ user_input - 1 ].split( '.' )[ 0 ]
    return file_name

# Retrieves palette name as a string
def get_palette_name():
    while True:
        try:
            palette_name = file_name_selection( palette_file_list, 'Enter file number: ')
            break
        except ValueError:
            print( 'Invalid input\n' )
        except IndexError:
            print( 'Invalid number\n' )
    return palette_name

# Retrieves palette color dictionary from user input
def get_palette_colors( palette_name ):
    palette_directory = 'palettes.' + palette_name
    palette_module = importlib.import_module( palette_directory )
    return palette_module.colors

# Adds additional color gradient
def advanced_colors( colors ):
    color_list = []
    for color in colors.keys():
        color_list.append( color )
    for color in color_list:
        new_color = ( int( color[0] * 220 / 255 ), int( color[1] * 220 / 255 ), int( color[2] * 220 / 255 ), 255 )
        colors[ new_color ] = colors[ color ] + '#2'
        new_color = ( int( color[0] * 180 / 255 ), int( color[1] * 180 / 255 ), int( color[2] * 180 / 255 ), 255 )
        colors[ new_color ] = colors[ color ] + '#1'
        colors[ color ] = colors[ color ] + '#3'
    return colors

# Retrieves operation mode from user input
def get_op_mode() -> int:
    while True:
        try:
            print()
            print('1: Scale image')
            print('2: Convert image to color palette')
            print('3: Reduce total number of colors')
            print('4: Generate outline')
            print('5: Print block counts')
            print('6: Print column data')
            print('7: Export color sections')
            print('8: (Minecraft Only) Print Minecraft function build script')
            user_input = int( input( '\nSelect the mode of operation: ' ) )
            if ( user_input >= 1 and user_input <= 7 ) or user_input == 8 and palette_name[ 0 ] == 'M':
                break
            else:
                raise IndexError
        except ValueError:
            print( 'Invalid input\n' )
        except IndexError:
            print( 'Invalid number\n' )
    return user_input
        
# Retrieves file name from user input
def get_file_name( dir_list, dir_str, prompt ):
    while True:
        try:
            im_name = file_name_selection( dir_list, prompt )
            break      
        except ValueError:
            print('Invalid input\n')
        except IndexError:
            print('Invalid number\n') 
    return im_name

# Defines color matcher
def color_match( color_pixel_1, color_pixel_2 ):
    color_pixel_1_sRGB = sRGBColor( color_pixel_1[0], color_pixel_1[1], color_pixel_1[2], True )
    color_pixel_2_sRGB = sRGBColor( color_pixel_2[0], color_pixel_2[1], color_pixel_2[2], True )

    color_pixel_1_lab = convert_color( color_pixel_1_sRGB, LabColor )
    color_pixel_2_lab = convert_color( color_pixel_2_sRGB, LabColor )

    delta_e = delta_e_cie2000( color_pixel_1_lab, color_pixel_2_lab )
    return delta_e

def im_save_file( im, dir_name, file_name, file_type ):
    # Failsafe if folder '1_scaled' is deleted
    try:
        im.save( dir_name + '/' + file_name, file_type )
    except FileNotFoundError:
        os.mkdir( dir_name )
        im.save( dir_name + '/' + file_name, file_type )    

def im_scale( im, size ):
    im_converted = im
    return im_converted

def mode_select() -> int:
    if palette_name == 'MinecraftMapColorsCreative' or palette_name == 'MinecraftMapColorsSurvival':
        while True:
            try:
                print()
                print('1: Standard (2D)')
                print('2: Advanced (Staircase)')
                flatten_img_input = int( input( '\nSelect build method: ' ) )
                if flatten_img_input >= 1 and flatten_img_input <= 2:
                    break
                else:
                    raise IndexError
            except ValueError:
                print( 'Invalid input\n' )
            except IndexError:
                print( 'Invalid number\n' )
    else:
        flatten_img_input = 3
    return flatten_img_input

def im_limit_colors( im, size, flatten_img_input ):
    block_colors = colors.keys()
    block_names = []
    for block_color in block_colors:
        block_names.append( colors[ block_color ] )

    # Converts image to palette-compliant colors
    im_converted = im
    full_color_image = im_converted.load()

    x = 0
    y = 0
    while y < size[1]:
        while x < size[0]:
            pixel = im.getpixel((x, y))
            if pixel in colors:
                dict_key = pixel
            else:
                min = 100
                for key in block_colors:
                    new_match = color_match(pixel, key)
                    if new_match < min:
                        min = new_match
                        dict_key = key
            if( flatten_img_input == 1 ):
                block_base = colors[ dict_key ].split( '#' )[ 0 ]
                block_variant = colors[ dict_key ].split( '#' )[ 1 ]
                if( block_variant == 1 or block_variant == 3 ):
                    dict_key = block_colors[ block_names.index( block_base + '#2' ) ]
            full_color_image[x, y] = dict_key
            print( 'Pixel', x, '|', y, 'changed to', dict_key )
            x = x + 1
        y = y + 1
        x = 0   
    return im_converted

def im_reduce_colors( im, size ):
    colors_reduced = False
    min_gem_count = 1

    # Prompt for color limit thresholds
    while True:
        try:
            count_threshold = int( input( '\nEnter minimum blocks per color: ') )
            if count_threshold < 1 or count_threshold > ( size[0] * size[1] ):
                raise IndexError
            else:
                break
        except ValueError:
            print( 'Invalid entry; enter a number between 1 and {}'.format( size[0] * size[1] ) )
        except IndexError:
            print( 'Invalid entry; enter a number between 1 and {}'.format( size[0] * size[1] ) )

    while True:
        try:
            block_type_threshold = int( input( 'Enter preferred number of colors: ') )
            if block_type_threshold < 1 or block_type_threshold > ( len( colors.keys() ) ):
                raise IndexError
            else:
                break
        except ValueError:
            print( 'Invalid entry; enter a number between 1 and {}'.format( len( colors.keys() ) ) )
        except IndexError:
            print( 'Invalid entry; enter a number between 1 and {}'.format( len( colors.keys() ) ) )

    flatten_img_input = mode_select()

    while not colors_reduced:
        x = 0
        y = 0
        pixel = ''
        color_code_row = []
        color_code_map = []
        color_val_list = []
        color_counts = []

        while y < size [ 1 ]:
            if x < size[ 0 ]:
                pixel = im.getpixel( ( x, y ) )
                color_code_row.append( pixel )
                x += 1
            else:
                color_code_map.append( color_code_row )
                color_code_row = []
                y += 1
                x = 0

        color_code_list = im.getcolors()

        for color_code in color_code_list:
            for term in color_code:
                if type( term ) == int:
                    num = term
                    color_counts.append( num )
                elif type( term ) == tuple:
                    color_val_list.append( term )
                    if flatten_img_input == 2:
                        if term not in colors:
                            var_1 = ( term[0] * 255 / 220, term[1] * 255 / 220, term[2] * 255 / 220, term[3] )
                            var_3 = ( term[0] * 255 / 180, term[1] * 255 / 220, term[2] * 255 / 220, term[3] )
                            if var_1 in colors:
                                color_counts[ color_val_list.index( var_1 ) ] += color_counts.pop()
                            elif var_3 in colors:
                                color_counts[ color_val_list.index( var_1 ) ] += color_counts.pop()
                            color_val_list.pop()

        if len( color_val_list ) < block_type_threshold or min_gem_count > count_threshold:
            colors_reduced = True
        else:
            for x in range( 0, len( color_val_list ) ):
                if color_counts[ x ] < min_gem_count:
                    del colors[ color_val_list[ x ] ]
                    im = im_limit_colors( im, size, flatten_img_input )
        min_gem_count += 1
        # print(min_gem_count)
    return im

def im_rm_fill( im, size ):
    row = []
    map = []
    x = 0
    y = 0
    pixel = ''

    # Converts array of color codes to array of block names
    while y < size[1]:
        if x < size[0]:
            pixel = im.getpixel( ( x, y ) )
            if pixel in colors:
                block = colors[ pixel ]
            else:
                block = 'NULL'
            row.append( block )
            x = x + 1
        else:
            map.append(row)
            row = []
            y = y + 1
            x = 0

    # Draws the outline of each uninterrupted color section
    build_key_image = im
    build_key = map
    build_key_edited = build_key_image.load()
    on_off = 0
    print('Size:', size[0], 'x', size[1])
    for ypos in range(len(build_key)):
        if on_off == 0:
            on_off += 1
        else:
            on_off -= 1
        for xpos in range(len(build_key[ypos])):
            block = build_key[ypos][xpos]
            if on_off == 0:
                on_off += 1
            else:
                on_off -= 1
            if ypos == 0 or ypos == (len(build_key) - 1) or xpos == 0 or xpos == (len(build_key[0]) - 1):
                continue
            else:
                if block == build_key[ypos+1][xpos]:
                    if block == build_key[ypos-1][xpos]:
                        if block == build_key[ypos][xpos+1]:
                            if block == build_key[ypos][xpos-1]:
                                # Replaces transparent background with semi-transparent checkerboard pattern
                                if on_off == 0:
                                    build_key_edited[xpos, ypos] = (32, 32, 32, 64)
                                else:
                                    build_key_edited[xpos, ypos] = (128, 128, 128, 64)
    return build_key_image

def im_print_block_counts( im, size ):
    color_code_list = im.getcolors()

    # Displays number of blocks needed
    num = 0
    index = 0
    color_keys = []
    block_counts = []
    block_list = []
    for c in colors.keys():
        color_keys.append(c)

    for color_code in color_code_list:
        num = color_code[ 0 ]
        block_counts.append( num )
        rgb_code = color_code[ 1 ]
        if( rgb_code in color_keys ):
            block_name = colors[ rgb_code ].split( '#' )[ 0 ]
            if( block_name in block_list ):
                merge_index = block_list.index( block_name )
                block_counts[ merge_index ] += block_counts[ index ]
                block_counts.pop()
            else:
                block_list.append( block_name )
                index += 1
        else: 
            block_list.append( rgb_code )
            index += 1

    output_list = []
    for x in range(0, len( block_counts ) ):
        output_list.append( '{}: {}'.format( block_list[ x ], block_counts[ x ] ) )
    output_list.sort()
    for str in output_list:
        print( str )
    print()

def im_print_col_data( im, size ):
    while True:
        try:
            print()
            col_num = int( input( '\nEnter column number ({} to {}): '.format( 1, size[0] ) ) )
            if col_num >= 1 and col_num <= size[0]:
                break
            else:
                raise IndexError
        except ValueError:
            print( 'Invalid input\n' )
        except IndexError:
            print( 'Invalid number\n' )
    x = col_num - 1

    print( 'Displaying column top-to-bottom' )
    count = 0
    stair_up = -1
    prev_color = colors[ im.getpixel( ( x, 0 ) ) ]
    for y in range( size[1] ):
        curr_color = colors[ im.getpixel( ( x, y ) ) ]
        if( curr_color == prev_color ):
            count += 1
        else:
            print( prev_color, '({})'.format( count ) )
            count = 1
        if( int( curr_color[ -1 ] ) != stair_up and int( curr_color[ -1 ] ) != 2 ):
            print()
            stair_up = int( curr_color[ -1 ] )
        prev_color = curr_color
    print( prev_color, '({})'.format( count ) )
    return

def im_print_color_sections( im, size ):
    print( '{} x {}'.format( size[ 0 ], size[ 1 ] ) )
    color_code_list = im.getcolors()

    # Displays number of blocks needed
    color_keys = []
    for c in colors.keys():
        color_keys.append(c)

    for color_code in color_code_list:
        base_img = im.copy()
        color = color_code[ 1 ]
        new_image = im_print_color( base_img, size, color )
        save_dir_str = '{}/5_colors/{}/'.format( palette_name, no_extension )
        if( color in colors ):
            file_str = '{}.png'.format( colors[ color ] )
        else:
            file_str = '{}.png'.format( color )
        im_save_file( new_image, save_dir_str, file_str, 'PNG' )

def im_print_color( im, size, color ):
    row = []
    map = []
    x = 0
    y = 0
    pixel = ''

    # Converts array of color codes to array of block names
    while y < size[1]:
        if x < size[0]:
            pixel = im.getpixel( ( x, y ) )
            if pixel == color:
                block = pixel
            else:
                block = 'NULL'
            row.append( block )
            x = x + 1
        else:
            map.append(row)
            row = []
            y = y + 1
            x = 0

    # Draws the outline of each uninterrupted color section
    build_key_image = im
    build_key = map
    build_key_edited = build_key_image.load()
    on_off = 0
    for ypos in range(len(build_key)):
        if on_off == 0:
            on_off += 1
        else:
            on_off -= 1
        for xpos in range(len(build_key[ypos])):
            block = build_key[ypos][xpos]
            if on_off == 0:
                on_off += 1
            else:
                on_off -= 1
            if block != 'NULL':
                continue
            else:
                # Replaces transparent background with semi-transparent checkerboard pattern
                if on_off == 0:
                    build_key_edited[xpos, ypos] = (32, 32, 32, 64)
                else:
                    build_key_edited[xpos, ypos] = (128, 128, 128, 64)
    return build_key_image

def im_print_build_function( im, size, palette_name, no_extension ):
    print( 'Printing Datapack function...' )

    # Create script file
    try:
        f = open( palette_name + '/build_scripts/' + no_extension.lower() + '.mcfunction', 'w' )
    except FileNotFoundError:
        os.mkdir( palette_name + '/build_scripts' )
        f = open( palette_name + '/build_scripts/' + no_extension.lower() + '.mcfunction', 'w' )

    for x in range( size[0] ):
        stair_up = -1
        segment_list = []
        segment = []
        segment.append( colors[ (112,112,112,255) ] )
        for z in range( size[1] ):
            curr_color = colors[ im.getpixel( ( x, z ) ) ]
            if( int( curr_color[ -1 ] ) != stair_up and int( curr_color[ -1 ] ) != 2 ):
                segment_list.append( [ segment, stair_up, x, z ] )
                segment = []
                stair_up = int( curr_color[ -1 ] )
            segment.append( curr_color )
        segment_list.append( [ segment, stair_up, x, z ] )
        print_segments( segment_list, f )
    return

def print_segments( segment_list, file ):
    segment_list.reverse()
    prev_seg_length = 0
    for seg in segment_list:
        seg[ 0 ].reverse()
        stair_dir = seg[ 1 ]
        x = seg[ 2 ]
        z = seg[ 3 ]
        if( stair_dir == 1 ):
            y = 0
        else:
            y = max( len( seg[ 0 ] ), prev_seg_length )
        prev_block = 2
        for color in seg[ 0 ]:
            block_txt = color.split('#')[ 0 ].split()[ 2 ].lower()
            if( prev_block != 2 ):
                # Build upwards
                if( stair_dir == 1 ):
                    y += 1
                # Build downwards
                else:
                    y -= 1
            if( block_txt == 'fire' ):
                file.write( 'setblock ~{} ~{} ~{} minecraft:{}\n'.format( x, y-1, z, 'netherrack' ) )
            elif( block_txt == 'water' or block_txt == 'lava' ):
                file.write( 'setblock ~{} ~{} ~{} minecraft:{}\n'.format( x, y-1, z, 'barrier' ) )
                file.write( 'setblock ~{} ~{} ~{} minecraft:{}\n'.format( x-1, y, z, 'barrier' ) )
                file.write( 'setblock ~{} ~{} ~{} minecraft:{}\n'.format( x+1, y, z, 'barrier' ) )
                file.write( 'setblock ~{} ~{} ~{} minecraft:{}\n'.format( x, y, z-1, 'barrier' ) )
                file.write( 'setblock ~{} ~{} ~{} minecraft:{}\n'.format( x, y, z+1, 'barrier' ) )
            file.write( 'setblock ~{} ~{} ~{} minecraft:{}\n'.format( x, y, z, block_txt ) )
            prev_block = int( color[ -1 ] )
            z -= 1
        prev_seg_length = len( seg[ 0 ] )
    return

#-------------------------------#
#         Main function         #
#-------------------------------#
if __name__ == '__main__':
    # Local variables
    full_convert = False    # Toggle single-/multi-file conversion

    # Set up patch_asscalar callback
    setattr( numpy, "asscalar", patch_asscalar )

    # Load in contents of 'palettes' folders
    palette_file_list = os.listdir( 'palettes' )
    palette_file_list = purge_directory( palette_file_list )
    palette_file_list.sort()

    # Get palette color from directory
    palette_name = get_palette_name()
    colors = get_palette_colors( palette_name )
    if( palette_name == 'MinecraftMapColorsCreative' or palette_name == 'MinecraftMapColorsSurvival' ):
        colors = advanced_colors( colors )

    try:
        os.mkdir( palette_name )
    except:
        pass
    try:
        os.mkdir( palette_name + '/0_source' )
    except:
        pass # Directory already exists
    try:
        os.mkdir( palette_name + '/1_scaled' )
    except:
        pass # Directory already exists
    try:
        os.mkdir( palette_name + '/2_color_lim' )
    except:
        pass # Directory already exists
    try:
        os.mkdir( palette_name + '/3_color_dec' )
    except:
        pass # Directory already exists
    try:
        os.mkdir( palette_name + '/4_outline' )
    except:
        pass # Directory already exists
    try:
        os.mkdir( palette_name + '/5_colors' )
    except:
        pass # Directory already exists

    # Load in contents of 'image' folders
    source_file_list = os.listdir( palette_name + '/0_source' )
    scaled_file_list = os.listdir( palette_name + '/1_scaled' )
    color_lim_file_list = os.listdir( palette_name + '/2_color_lim' )
    color_dec_file_list = os.listdir( palette_name + '/3_color_dec' )
    outline_file_list = os.listdir( palette_name + '/4_outline' )

    # Clean out temporary files
    source_file_list = purge_directory( source_file_list )
    scaled_file_list = purge_directory( scaled_file_list )
    color_lim_file_list = purge_directory( color_lim_file_list )
    color_dec_file_list = purge_directory( color_dec_file_list )
    outline_file_list = purge_directory( outline_file_list )

    # Sort file lists
    source_file_list.sort()
    scaled_file_list.sort()
    color_lim_file_list.sort()
    color_dec_file_list.sort()
    outline_file_list.sort()

    # Add CONVERT ALL option to each list
    convert_all_str = 'CONVERT ALL'
    source_file_list.append( convert_all_str )
    scaled_file_list.append( convert_all_str )
    color_lim_file_list.append( convert_all_str )
    color_dec_file_list.append( convert_all_str )
    outline_file_list.append( convert_all_str )

    op_mode = get_op_mode()

    while( True ):
        if( op_mode == 1 ):
            current_dir_list = source_file_list
            dir_str = palette_name + '/0_source'
            old_extension = ''
            prompt = 'Enter file number to scale. Leave blank to scale all files in \'{}\': '.format( dir_str )
        elif( op_mode == 2 ):
            current_dir_list = scaled_file_list
            dir_str = palette_name + '/1_scaled'
            old_extension = '_Scaled'
            prompt = 'Enter file number to convert. Leave blank to convert all files in \'{}\': '.format( dir_str )
        elif( op_mode == 3 ):
            current_dir_list = color_lim_file_list
            dir_str = palette_name + '/2_color_lim'
            old_extension = '_Color_Lim'
            prompt = 'Enter file number to color reduce. Leave blank to operate on all files in \'{}\': '.format( dir_str )
        elif( op_mode == 4 ):
            current_dir_list = color_dec_file_list
            dir_str = palette_name + '/3_color_dec'
            old_extension = '_Color_Dec'
            prompt = 'Enter file number to outline. Leave blank to print all data in \'{}\': '.format( dir_str )
        elif( op_mode == 5 ):
            current_dir_list = color_dec_file_list
            dir_str = palette_name + '/3_color_dec'
            old_extension = '_Color_Dec'
            prompt = 'Enter file number to print block counts data for: '
        elif( op_mode == 6 ):
            current_dir_list = color_dec_file_list
            dir_str = palette_name + '/3_color_dec'
            old_extension = '_Color_Dec'
            prompt = 'Enter file number to print column data for: '
        elif( op_mode == 7 ):
            current_dir_list = color_dec_file_list
            dir_str = palette_name + '/3_color_dec'
            old_extension = '_Color_Dec'
            prompt = 'Enter file number to generate color sections for: '
        elif( op_mode == 8 ):
            current_dir_list = color_dec_file_list
            dir_str = palette_name + '/3_color_dec'
            old_extension = '_Color_Dec'
            prompt = 'Enter file number to generate build script for: '

        im_name = get_file_name( current_dir_list, dir_str, prompt )
        if im_name == 'CONVERT ALL':
            current_dir_list.remove( 'CONVERT ALL' )
            full_convert = True
            break
        else:
            try:
                im_name = im_name + '.png'
                im = Image.open( dir_str + '/' + im_name )
                break
            except:
                try:
                    im_name = im_name + '.jpg'
                    im = Image.open( dir_str + '/' + im_name )
                    break
                except:
                    print('Error: File not supported')

    if not full_convert:
        current_dir_list = [ im_name ]

    # Looped once for single file conversion
    for name in current_dir_list:
        im = Image.open( dir_str + '/' + name )
        im.putalpha( 255 )

        no_extension = name.split('.')[ 0 ]
        no_extension = no_extension.replace( old_extension, '' )
        size = im.size

        if( op_mode == 1 ):
            print( 'Scaling down image {}...'.format( no_extension ) )
            new_image = im_scale( im, size )
            im_save_file( new_image, palette_name + '/1_scaled', no_extension + '_Scaled.png', 'PNG' )
            print( 'Result exported to {}.'.format( palette_name + '/1_scaled/' + no_extension + '_Scaled.png' ) )

        elif( op_mode == 2 ):
            print( 'Converting image {} to palette...'.format( no_extension ) )
            new_image = im_limit_colors( im, size, mode_select() )
            im_save_file( new_image, palette_name + '/2_color_lim', no_extension + '_Color_Lim.png', 'PNG' )
            print( 'Result exported to {}.'.format( palette_name + '/2_color_lim/' + no_extension + '_Color_Lim.png' ) )

        elif( op_mode == 3 ):
            print( 'Reducing colors in image {}...'.format( no_extension ) )
            new_image = im_reduce_colors( im, size )
            im_save_file( new_image, palette_name + '/3_color_dec', no_extension + '_Color_Dec.png', 'PNG' )
            print( 'Result exported to {}.'.format( palette_name + '/3_color_dec/' + no_extension + '_Color_Dec.png' ) )

        elif( op_mode == 4 ):
            print( 'Generating build guide for image {}...'.format( no_extension ) )
            new_image = im_rm_fill( im, size )
            im_save_file( new_image, palette_name + '/4_outline', no_extension + '_Outline.png', 'PNG' )
            print( 'Result exported to {}.'.format( palette_name + '/4_outline/' + no_extension + '_Outline.png' ) )

        elif( op_mode == 5 ):
            im_print_block_counts( im, size )

        elif( op_mode == 6 ):
            im_print_col_data( im, size )

        elif( op_mode == 7 ):
            print( 'Generating color files for image {}...'.format( no_extension ) )
            im_print_color_sections( im, size )
            print( 'Results exported to {}.'.format( palette_name + '/5_colors/' + no_extension + '/' ) )

        elif( op_mode == 8 ):
            im_print_build_function( im, size, palette_name, no_extension )
            print( 'Build function exported to {}.'.format( palette_name + '/build_scripts/' + no_extension + '/' ) )
